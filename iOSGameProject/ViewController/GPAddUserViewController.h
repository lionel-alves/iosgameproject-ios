//
//  GPAddUserViewController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 04/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddUserViewControllerDelegate;

@interface GPAddUserViewController : UITableViewController<UITextFieldDelegate>
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

@property (weak, nonatomic) id<AddUserViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *pseudoTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@protocol AddUserViewControllerDelegate <NSObject>

-(void)addUserViewControllerDidFinish:
(GPAddUserViewController *)controller name:(NSString *)name;

@end