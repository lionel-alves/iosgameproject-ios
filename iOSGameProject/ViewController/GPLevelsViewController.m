//
//  GPLevelsViewController.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPLevelsViewController.h"
#import "User.h"
#import "Score.h"
#import "GPAppDelegate.h"
#import "GameDataController.h"
#import "GPGameViewController.h"

@interface GPLevelsViewController ()

@end

@implementation GPLevelsViewController
@synthesize level1Button;
@synthesize level2Button;
@synthesize level3Button;
@synthesize totalScoreTitle;
@synthesize totalScoreLabel;
@synthesize nextLevelTitle;
@synthesize nextLevelLabel;
@synthesize currentUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.currentUser = [(GPAppDelegate *)[UIApplication sharedApplication].delegate currentUser];
    level1Button.enabled = YES;
    
    if (self.currentUser == nil)
    {
        [totalScoreTitle setHidden:YES];
        [totalScoreLabel setHidden:YES];
        [nextLevelTitle setHidden:YES];
        [nextLevelLabel setHidden:YES];
    }
    else
    {
        [totalScoreLabel setText:[currentUser.totalScore stringValue]];
        // Enable/Disable level Button
        if (self.currentUser.level == [NSNumber numberWithInteger:1])
        {
            level2Button.enabled = NO;
            level3Button.enabled = NO;
            [nextLevelLabel setText:[NSString stringWithFormat:@"%i", kLevel2Score]];
        }
        else if (self.currentUser.level == [NSNumber numberWithInteger:2])
        {
            level2Button.enabled = YES;
            level3Button.enabled = NO;
            [nextLevelLabel setText:[NSString stringWithFormat:@"%i", kLevel3Score]];
        }
        else if (self.currentUser.level == [NSNumber numberWithInteger:3])
        {
            level2Button.enabled = YES;
            level3Button.enabled = YES;
        }
    }

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)viewDidUnload
{
    [self setLevel1Button:nil];
    [self setLevel2Button:nil];
    [self setLevel3Button:nil];
    [self setTotalScoreTitle:nil];
    [self setTotalScoreLabel:nil];
    [self setNextLevelTitle:nil];
    [self setNextLevelLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showGameLevel1"]) {
        GPGameViewController *gameViewController = (GPGameViewController *)[segue destinationViewController];
        gameViewController.level = GPLevelEnum1;
    }
    else if ([[segue identifier] isEqualToString:@"showGameLevel2"]) {
        GPGameViewController *gameViewController = (GPGameViewController *)[segue destinationViewController];
        gameViewController.level = GPLevelEnum2;
    }
    else if ([[segue identifier] isEqualToString:@"showGameLevel3"]) {
        GPGameViewController *gameViewController = (GPGameViewController *)[segue destinationViewController];
        gameViewController.level = GPLevelEnum3;
    }
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
