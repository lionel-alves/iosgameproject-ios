//
//  GPUserViewController.h
//  iOSGameProject
//
//  Created by Lionel ALVES on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMGridView.h"
#import "GameDataController.h"
#import "GPAddUserViewController.h"

@class  GPAddUserViewController;

@interface GPUserViewController : UIViewController<MMGridViewDataSource, MMGridViewDelegate, AddUserViewControllerDelegate>
{
    IBOutlet MMGridView *gridView;
    IBOutlet UIPageControl *pageControl;
}

@property (strong, nonatomic) GameDataController *dataController;
- (IBAction)backButtonPressed:(id)sender;

@end

