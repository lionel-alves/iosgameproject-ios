//
//  GPLevelsViewController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;

@interface GPLevelsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *level1Button;
@property (weak, nonatomic) IBOutlet UIButton *level2Button;
@property (weak, nonatomic) IBOutlet UIButton *level3Button;
@property (weak, nonatomic) IBOutlet UILabel *totalScoreTitle;
@property (weak, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextLevelTitle;
@property (weak, nonatomic) IBOutlet UILabel *nextLevelLabel;

@property (strong, nonatomic) User *currentUser;
- (IBAction)backButtonPressed:(id)sender;


@end
