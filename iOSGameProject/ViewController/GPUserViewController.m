//
//  GPUserViewController.m
//  iOSGameProject
//
//  Created by Lionel ALVES on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPUserViewController.h"

#import "MMGridViewDefaultCell.h"
#import "GPGameViewController.h"
#import "GameDataController.h"
#import "GPAppDelegate.h"
#import "User.h"

@interface GPUserViewController ()
{

}

- (void)setupPageControl;

@end

@implementation GPUserViewController
@synthesize dataController = _dataController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    NSManagedObjectContext *context = [(GPAppDelegate *)[UIApplication sharedApplication].delegate managedObjectContext];
    if (!context) {
        // Handle the error.
    }
    */
    self.dataController = [(GPAppDelegate *)[UIApplication sharedApplication].delegate dataController];
    
    [self setupPageControl];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)addUserViewControllerDidFinish:
(GPAddUserViewController *)controller name:(NSString *)name {
    if ([name length]) {
        [_dataController addUserWithName:name];
        [self setupPageControl];
        [gridView reloadData];
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:
(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showLevel"]) {
        
    }
    else if ([[segue identifier]
              isEqualToString:@"showAddUser"]) {
        GPAddUserViewController *addController =
        (GPAddUserViewController *)[[[segue destinationViewController]
                                       viewControllers] objectAtIndex:0];
        addController.delegate = self;
    }
}

#pragma mark - internal functions

- (void)setupPageControl
{
    pageControl.numberOfPages = gridView.numberOfPages;
    pageControl.currentPage = gridView.currentPageIndex;
}

// ----------------------------------------------------------------------------------

#pragma - MMGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(MMGridView *)gridView
{
    return [self.dataController.userList count] + 1;
}


- (MMGridViewCell *)gridView:(MMGridView *)gridView cellAtIndex:(NSUInteger)index
{
    MMGridViewDefaultCell *cell = [[MMGridViewDefaultCell alloc] initWithFrame:CGRectNull];
    if (index == 0)
    {
        cell.textLabel.text = @"Ajouter un utilisateur";
        cell.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cell-plus.png"]];
    }
    else
    {
        
        User *tmpUser = [self.dataController.userList objectAtIndex:index - 1];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", tmpUser.name];
        cell.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cell-image.png"]];
    }
    
    return cell;
}

// ----------------------------------------------------------------------------------

#pragma - MMGridViewDelegate

- (void)gridView:(MMGridView *)gridView didSelectCell:(MMGridViewCell *)cell atIndex:(NSUInteger)index
{
    if (index == 0)
    {
        [self performSegueWithIdentifier:@"showAddUser" sender:nil];
    }
    else
    {
        [(GPAppDelegate *)[UIApplication sharedApplication].delegate setCurrentUser:[self.dataController.userList objectAtIndex:index - 1]];
        [self performSegueWithIdentifier:@"showLevel" sender:nil];
    }
}


- (void)gridView:(MMGridView *)gridView didDoubleTapCell:(MMGridViewCell *)cell atIndex:(NSUInteger)index
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:[NSString stringWithFormat:@"Cell at index %d was double tapped.", index]
                                                   delegate:nil 
                                          cancelButtonTitle:@"Cool!" 
                                          otherButtonTitles:nil];
    [alert show];

}


- (void)gridView:(MMGridView *)theGridView changedPageToIndex:(NSUInteger)index
{
    [self setupPageControl];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
