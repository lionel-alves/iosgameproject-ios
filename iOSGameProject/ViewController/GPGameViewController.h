//
//  GPGameViewController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 04/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class User;
@class GPGameView;

#define kNbQuestion 15

#define kNbLineLvl1 2
#define kNbColLvl1 1

#define kNbLineLvl2 4
#define kNbColLvl2 2

#define kNbLineLvl3 6
#define kNbColLvl3 4

#define kNbAnswersLvl1 (kNbColLvl1 * kNbLineLvl1)
#define kNbAnswersLvl2 (kNbColLvl2 * kNbLineLvl2)
#define kNbAnswersLvl3 (kNbColLvl3 * kNbLineLvl3)

typedef enum levelEnum {
    GPLevelEnum1 = 1,
    GPLevelEnum2 = 2,
    GPLevelEnum3 = 3
} GPLevelEnum;


@protocol GPGameDelegate <NSObject>

- (void) chooseAnswer:(NSInteger)answer;

@end

@interface GPGameViewController : UIViewController <GPGameDelegate>

@property (nonatomic) GPLevelEnum level;



@property (weak, nonatomic) IBOutlet GPGameView *gameView;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *indexQuestionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *answerIsGoodImage;

@end
