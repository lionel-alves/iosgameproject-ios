//
//  GPGameViewController.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 04/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "GPGameViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "GPResultGameViewController.h"
#import "../Model/Score.h"
#import "../GPAppDelegate.h"
#import "../View/GPGameView.h"
#import "../Model/GPQuestion.h"





@interface GPGameViewController ()
@property (weak, nonatomic) User *currentUser;

@property (nonatomic) NSInteger score;
@property (nonatomic) NSInteger indexQuestion;
@property (strong, nonatomic) GPQuestion *currentQuestion;
@property (nonatomic) NSInteger goodAnswers;
@end

@interface GPGameViewController (internalGame)

- (void) setupNextQuestion;

@end

@implementation GPGameViewController

@synthesize level;

@synthesize currentUser;
@synthesize gameView;
@synthesize questionLabel;
@synthesize scoreLabel;
@synthesize indexQuestionLabel;
@synthesize answerIsGoodImage;
@synthesize score;
@synthesize indexQuestion;
@synthesize currentQuestion;
@synthesize goodAnswers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [gameView setGameDelegate:self];
    
    indexQuestion = 0;
    goodAnswers = 0;
    self.currentUser = [(GPAppDelegate *)[UIApplication sharedApplication].delegate currentUser];
    
    score = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNextQuestion];
    [scoreLabel setText:@"0"];
    
}

- (void)viewDidUnload
{
    [self setGameView:nil];
    [self setQuestionLabel:nil];
    [self setScoreLabel:nil];
    [self setIndexQuestionLabel:nil];
    [self setAnswerIsGoodImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:
(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showResultGame"]) {
        GPResultGameViewController *gameResultController = (GPResultGameViewController *)[segue destinationViewController];
        if (level == GPLevelEnum1)
            gameResultController.currentScore = goodAnswers * 50;
        else if (level == GPLevelEnum2)
            gameResultController.currentScore = goodAnswers * 100;
        else if (level == GPLevelEnum3)
            gameResultController.currentScore = goodAnswers * 200;
        
        gameResultController.nbGoodAnswers = goodAnswers;
        gameResultController.currentUser = currentUser;
    }
}


#pragma mark - GPGameDelegate
- (void) chooseAnswer:(NSInteger)answer
{
    if (currentQuestion.result == answer)
    {
        goodAnswers++;
        if (level == GPLevelEnum1)
            score += 50;
        else if (level == GPLevelEnum2)
            score += 100;
        else if (level == GPLevelEnum3)
            score += 200;
        [scoreLabel setText:[NSString stringWithFormat:@"%i", score]];
        [answerIsGoodImage setImage:[UIImage imageNamed:@"Good.png"]];
    }
    else {
        [answerIsGoodImage setImage:[UIImage imageNamed:@"Wrong.png"]];
    }
         
    if (indexQuestion < kNbQuestion)
    {
        [self setupNextQuestion];
    }
    else
    {
        [self performSegueWithIdentifier:@"showResultGame" sender:nil];
    }    
}
@end

@implementation GPGameViewController (internalGame)

- (void) setupNextQuestion;
{
    currentQuestion = [[GPQuestion alloc] initWithLevel:level];
    [questionLabel setText:[currentQuestion description]];
    if (level == GPLevelEnum1)
        [gameView setCurrentAnswer:currentQuestion.result WithNbLine:kNbLineLvl1 AndNbCol:kNbColLvl1];
    else if (level == GPLevelEnum2)
        [gameView setCurrentAnswer:currentQuestion.result WithNbLine:kNbLineLvl2 AndNbCol:kNbColLvl2];
    else if (level == GPLevelEnum3)
        [gameView setCurrentAnswer:currentQuestion.result WithNbLine:kNbLineLvl3 AndNbCol:kNbColLvl3];
    
    indexQuestion++;
    [indexQuestionLabel setText:[NSString stringWithFormat:@"%i / %i", indexQuestion, kNbQuestion]];
}

@end