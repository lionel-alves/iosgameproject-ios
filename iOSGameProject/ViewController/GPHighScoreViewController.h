//
//  GPHighScoreViewController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class GameDataController;

@interface GPHighScoreViewController : UITableViewController

@property (strong, nonatomic) GameDataController *dataController;

- (IBAction)doneButtonPressed:(id)sender;

@end
