//
//  GPResultGameViewController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Score;
@class User;

@interface GPResultGameViewController : UIViewController

@property (nonatomic) NSInteger currentScore;
@property (weak, nonatomic) User *currentUser;
@property (nonatomic) NSInteger nbGoodAnswers;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
//- (IBAction)menuButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalScoreTitle;

@end
