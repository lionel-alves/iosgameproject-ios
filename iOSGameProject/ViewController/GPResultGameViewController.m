//
//  GPResultGameViewController.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPResultGameViewController.h"
#import "User.h"
#import "Score.h"
#import "GPAppDelegate.h"
#import "GameDataController.h"

@interface GPResultGameViewController ()

@end

@implementation GPResultGameViewController
@synthesize totalScoreLabel;
@synthesize totalScoreTitle;
@synthesize nbGoodAnswers;
@synthesize currentScore;
@synthesize currentUser;
@synthesize nameLabel;
@synthesize scoreLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.currentUser = [(GPAppDelegate *)[UIApplication sharedApplication].delegate currentUser];
    
    if (currentUser == nil)
        [nameLabel setText:@"Invité"];
    else
        [nameLabel setText:currentUser.name];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [scoreLabel setText:[NSString stringWithFormat:@"%i (%i / 15)", currentScore, nbGoodAnswers]];
    
    if (self.currentUser)
    {
        GameDataController *dataController = [(GPAppDelegate *)[UIApplication sharedApplication].delegate dataController];
        int levelup = [dataController addScoreWithUser:currentUser andScore:currentScore];
        if (levelup == 0)
            [totalScoreLabel setText:[self.currentUser.totalScore stringValue]];
        else
            [totalScoreLabel setText:[NSString stringWithFormat:@"%@ -> Lvl %i débloqué !",[self.currentUser.totalScore stringValue], levelup]];
    }
    else
    {
        [totalScoreLabel setHidden:YES];
        [totalScoreTitle setHidden:YES];
    }
         
}

- (void)viewDidUnload
{
    [self setNameLabel:nil];
    [self setScoreLabel:nil];
    [self setTotalScoreLabel:nil];
    [self setTotalScoreTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//- (IBAction)menuButtonPressed:(id)sender {
//    [self.navigationController setViewControllers:[NSArray arrayWithObjects:[[self.navigationController viewControllers] objectAtIndex:0], nil]];
//}
@end
