//
//  GPAddUserViewController.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 04/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPAddUserViewController.h"

@interface GPAddUserViewController ()

@end

@implementation GPAddUserViewController

@synthesize pseudoTextField = _pseudoTextField;
@synthesize errorLabel = _errorLabel;
@synthesize delegate = _delegate;

- (void)awakeFromNib
{
    UIColor *color = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"MathsKillerBg.jpg"]];
    self.view.backgroundColor = color;
    [super awakeFromNib];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [_pseudoTextField becomeFirstResponder];
    _pseudoTextField.delegate = self;
}

- (void)viewDidUnload
{
    [self setPseudoTextField:nil];
    [self setErrorLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)doneButtonPressed:(id)sender {
    if (![_pseudoTextField.text isEqualToString:@""])
        [[self delegate] addUserViewControllerDidFinish:self name:self.pseudoTextField.text];
    else
        [_errorLabel setHidden:NO];

}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.pseudoTextField) {
        if (![textField.text isEqualToString:@""])
            [self doneButtonPressed:nil];
        else
            [_errorLabel setHidden:NO];
    }
    return YES;
}

@end
