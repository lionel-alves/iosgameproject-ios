//
//  GPViewController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class MMGridView;

@interface GPViewController : UIViewController
{
    
}

@property (nonatomic, strong) IBOutlet UIButton *playPauseButton;
@property (nonatomic, strong) IBOutlet UISlider *volumeControl;
@property (nonatomic, strong) IBOutlet UILabel *alertLabel;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

- (IBAction)volumeDidChange:(id)slider; //handle the slider movement
- (IBAction)togglePlayingState:(id)button; //handle the button tapping

- (void)playAudio; //play the audio
- (void)pauseAudio; //pause the audio

- (void)togglePlayPause; //toggle the state of the audio


@end
