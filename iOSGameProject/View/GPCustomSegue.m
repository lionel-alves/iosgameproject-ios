//
//  GPCustomSegue.m
//  iOSGameProject
//
//  Created by Lionel ALVES on 11/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPCustomSegue.h"

@implementation GPCustomSegue

- (void) perform {

    UIViewController *src = (UIViewController *) self.sourceViewController;
    //UIViewController *dst = (UIViewController *) self.destinationViewController;
    

    [UIView transitionWithView:src.navigationController.view duration:0.4
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        [src.navigationController setViewControllers:[NSArray arrayWithObjects:[[src.navigationController viewControllers] objectAtIndex:0], nil]];
                        
                    }
                    completion:NULL];
    
}

@end
