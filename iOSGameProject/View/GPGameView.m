//
//  GPGameView.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPGameView.h"
#import "../ViewController/GPGameViewController.h"

@interface GPGameView ()

@property (nonatomic) NSInteger currentAnswer;
@property (strong, nonatomic) NSMutableArray *answers;
@property (nonatomic) NSInteger answerPos;
@property (nonatomic) NSInteger nbAnswer;
@property (nonatomic) CGFloat answerWidth;
@property (nonatomic) CGFloat answerHeight;
@property (nonatomic) NSInteger nbLine;
@property (nonatomic) NSInteger nbCol;
- (void)drawNumber:(NSNumber *)number InRect:(CGRect)rect;

@end

@implementation GPGameView

@synthesize gameDelegate;
@synthesize currentAnswer;
@synthesize answerPos;
@synthesize answers;
@synthesize nbAnswer;
@synthesize answerWidth, answerHeight;
@synthesize nbLine, nbCol;


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        nbAnswer = 0;
    }
    return self;
}

- (void)setCurrentAnswer:(NSInteger)myCurrentAnswer WithNbLine:(NSInteger)myNbLine AndNbCol:(NSInteger)myNbCol
{
    currentAnswer = myCurrentAnswer;
    
    nbLine = myNbLine;
    nbCol = myNbCol;
    
    nbAnswer = nbLine * nbCol;
    
    answerHeight = self.bounds.size.height / nbLine;
    answerWidth = self.bounds.size.width / nbCol;
    
    answers = [[NSMutableArray alloc] init];
    answerPos = arc4random() % nbAnswer;
    
    for (int i = 0; i < nbAnswer; i++) {
        if (answerPos == i)
            [answers insertObject:[NSNumber numberWithInteger:currentAnswer] atIndex:i];
        else
        {
            if (nbAnswer == kNbAnswersLvl1)
            {
                int tmpAnswer = currentAnswer - 10 + arc4random() % 20;
                if (tmpAnswer == currentAnswer)
                    tmpAnswer += 1;
                [answers insertObject:[NSNumber numberWithInteger:tmpAnswer] atIndex:i];
            }
            else if (nbAnswer == kNbAnswersLvl2)
            {
                int tmpAnswer = currentAnswer - 20 + arc4random() % 40;
                if (tmpAnswer == currentAnswer)
                    tmpAnswer += 1;
                [answers insertObject:[NSNumber numberWithInteger:tmpAnswer] atIndex:i];
            }
            else
            {
                int tmpAnswer = currentAnswer - 30 + arc4random() % 60;
                if (tmpAnswer == currentAnswer)
                    tmpAnswer += 1;
                [answers insertObject:[NSNumber numberWithInteger:tmpAnswer] atIndex:i];
            }
                
        }
    }
    [self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    NSInteger pos = floorf(point.x / answerWidth)  + nbCol * floorf(point.y / answerHeight);
    
    [gameDelegate chooseAnswer:[[answers objectAtIndex:pos] integerValue]];
    

}

- (void)drawNumber:(NSNumber *)number InRect:(CGRect)rect{
    CGContextRef context7 = UIGraphicsGetCurrentContext();

    CGContextSelectFont(context7, "Helvetica", 18, 
                        kCGEncodingMacRoman);
    //CGContextSetTextMatrix (context7, CGAffineTransformMake(1.0, 0.0, 
    //                                                        0.0, -1.0, 0.0, 0.0));

    
    
    CGFloat x = rect.origin.x;
    CGFloat y = rect.origin.y;
    
    NSString *theText = [NSString stringWithFormat:@"%d", [number integerValue]];
    CGSize labelSize = [theText sizeWithFont:[UIFont fontWithName:@"Helvetica" size:18]];
    
    
    if (nbAnswer == kNbAnswersLvl1)
    {
        CGContextSetTextMatrix (context7, CGAffineTransformMake(1.0, 0.0, 
                                                                0.0, -1.0, 0.0, 0.0));
        x = rect.origin.x + rect.size.width / 2 - labelSize.width / 2;
        y = rect.origin.y + rect.size.height / 2 + labelSize.height / 2;
    }
    else
    {
        int transform = arc4random() % 3;
        
        if (transform == 0)
        {
            CGContextSetTextMatrix (context7, CGAffineTransformMake(1.0, 0.0, 
                                                                    0.0, -1.0, 0.0, 0.0));
            x = rect.origin.x + rect.size.width / 2 - labelSize.width / 2;
            y = rect.origin.y + rect.size.height / 2 + labelSize.height / 2;
        }
        else if (transform == 1)
        {        
            CGContextSetTextMatrix(context7,
                               CGAffineTransformRotate(CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0), M_PI / 2));
            x = rect.origin.x + arc4random() % ((int)rect.size.width - (int)labelSize.height - 10) + labelSize.height / 2 + 5;
            y = rect.origin.y + arc4random() % ((int)rect.size.height - (int)labelSize.width - 20) + labelSize.width / 2 + 20;
        }  
        else if (transform == 2)
        {
           CGContextSetTextMatrix(context7,
                               CGAffineTransformRotate(CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0), M_PI / (- 2)));
            x = rect.origin.x + arc4random() % ((int)rect.size.width - (int)labelSize.height - 10) + labelSize.height / 2 + 5;
            y = rect.origin.y + arc4random() % ((int)rect.size.height - (int)labelSize.width - 20) + labelSize.width / 2;
        }
    }
        
    CGContextSetTextDrawingMode(context7, kCGTextFill);
    CGContextSetFillColorWithColor(context7, [[UIColor colorWithRed:0 
                                                              green:0 blue:0 alpha:1.0] CGColor]);

    
    CGContextShowTextAtPoint(context7, x, y,
                             [theText cStringUsingEncoding:NSUTF8StringEncoding], [theText length]);
}

- (UIColor *) randomColor
{
	CGFloat red =  0.1;
	CGFloat blue = 0.5 + ((CGFloat)random()/(CGFloat)RAND_MAX) / 2;
	CGFloat green = 0.4;
    CGFloat alpha = 0.5 + ((CGFloat)random()/(CGFloat)RAND_MAX) / 2;
	return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    int j = 0;
    for (int i = 0;  i < nbAnswer; i++) {
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(context, 2.0);
        CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
        CGRect rectangle = CGRectMake((i % nbCol) * answerWidth, j * answerHeight, answerWidth, answerHeight);
        CGContextAddRect(context, rectangle);
        CGContextStrokePath(context);
        CGContextSetFillColorWithColor(context, [self randomColor].CGColor);
        CGContextFillRect(context, rectangle);
        
        [self drawNumber:[answers objectAtIndex:i] InRect:rectangle];
    
        if (i % nbCol == nbCol - 1)
            j++;
    }
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGRect rectangle = CGRectMake(0, 0, 320, 360);
    CGContextAddRect(context, rectangle);
    CGContextStrokePath(context);
    CGContextSetFillColorWithColor(context, [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"MathsKillerGameBg.png"]].CGColor);
    CGContextFillRect(context, rectangle);

    
}

@end
