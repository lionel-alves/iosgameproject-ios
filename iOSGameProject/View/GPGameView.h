//
//  GPGameView.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GPGameDelegate;

@interface GPGameView : UIView

@property (weak, nonatomic) id<GPGameDelegate> gameDelegate;

- (void)setCurrentAnswer:(NSInteger)myCurrentAnswer WithNbLine:(NSInteger)nbLine AndNbCol:(NSInteger)nbCol;

@end
