//
//  GameDataController.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameDataController.h"
#import "User.h"
#import "Score.h"

@interface GameDataController ()
- (void)initializeDefaultDataList;

@end

@implementation GameDataController

@synthesize userList = _userList;
@synthesize scoreList = _scoreList;
@synthesize highScoreList = _highScoreList;

@synthesize managedObjectContext;

- (id)initWithContext:(NSManagedObjectContext *)context {
    if (self = [super init]) {
        self.managedObjectContext = context;
        [self initializeDefaultDataList];
        return self;
    }
    return nil;
}

- (void)initializeDefaultDataList {
    NSMutableArray *users = [[NSMutableArray alloc] init];
    NSMutableArray *scores = [[NSMutableArray alloc] init];
    NSMutableArray *highScores = [[NSMutableArray alloc] init];
    
    self.userList = users;
    self.scoreList = scores;
    self.highScoreList = highScores;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"User"
                                   inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc]
                                initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[managedObjectContext
                                            executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        // Handle the error.
    }
    
    [self setUserList:mutableFetchResults];
    
    NSFetchRequest *request2 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity2 = [NSEntityDescription
                                    entityForName:@"Score"
                                    inManagedObjectContext:managedObjectContext];
    [request2 setEntity:entity2];
    
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc]
                                        initWithKey:@"score" ascending:NO];
    NSArray *sortDescriptors2 = [[NSArray alloc]
                                initWithObjects:sortDescriptor2, nil];
    [request2 setSortDescriptors:sortDescriptors2];
    
    NSMutableArray *mutableFetchResults2 = [[managedObjectContext
                                             executeFetchRequest:request2 error:&error] mutableCopy];
    if (mutableFetchResults2 == nil) {
        // Handle the error.
    }
    
    [self setScoreList:mutableFetchResults2];
    
    NSFetchRequest *request3 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity3 = [NSEntityDescription
                                   entityForName:@"User"
                                   inManagedObjectContext:managedObjectContext];
    [request3 setEntity:entity3];
    
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc]
                                        initWithKey:@"totalScore" ascending:NO];
    NSArray *sortDescriptors3 = [[NSArray alloc]
                                initWithObjects:sortDescriptor3, nil];
    [request3 setSortDescriptors:sortDescriptors3];
    
    NSError *error3 = nil;
    NSMutableArray *mutableFetchResults3 = [[managedObjectContext
                                            executeFetchRequest:request3 error:&error3] mutableCopy];
    if (mutableFetchResults3 == nil) {
        // Handle the error.
    }
    
    [self setHighScoreList:mutableFetchResults3];

}

- (void)setUserList:(NSMutableArray *)newList {
    if (_userList != newList) {
        _userList = [newList mutableCopy];
    }
}

- (void)setScoreList:(NSMutableArray *)newList {
    if (_scoreList != newList) {
        _scoreList = [newList mutableCopy];
    }
}

-(NSUInteger)countOfList {
    return [self.userList count];
}

-(User *)objectInListAtIndex:(NSUInteger)theIndex{
    return [self.userList objectAtIndex:theIndex];
}

-(void)addUserWithName:(NSString *)inputUserName {
    
    // Create and configure a new instance of the User entity.
    User *user = (User *)[NSEntityDescription
                          insertNewObjectForEntityForName:@"User"
                          inManagedObjectContext:managedObjectContext];
    [user setName:inputUserName];
    [user setLevel:[NSNumber numberWithInt:1]];
    [user setTotalScore:[NSNumber numberWithInt:0]];
    
    [self.userList insertObject:user atIndex:0];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        //Handle the error
    }
}

-(void)deleteUserWithIndexPath:(NSIndexPath *)indexPath {
    // Delete the managed object at the given index path.
    NSManagedObject *userToDelete = [_userList objectAtIndex:indexPath.row];
    [managedObjectContext deleteObject:userToDelete];
    // Update the array and table view.
    [_userList removeObjectAtIndex:indexPath.row];
    
    // Commit the change.
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        //handle error
    }
}

-(int)addScoreWithUser:(User *)currentUser andScore:(NSInteger) myScore {
    int res = 0;
    NSDate *today = [NSDate date];
    
    // Create and configure a new instance of the User entity.
    Score *score = (Score *)[NSEntityDescription
                          insertNewObjectForEntityForName:@"Score"
                          inManagedObjectContext:managedObjectContext];
    [score setDate:today];
    [score setScore:[NSNumber numberWithInteger:myScore]];
    [score setUser:currentUser];

    
    
    
    for (int i = 0; i < [self.scoreList count]; i++) {
        if ([((Score *)[self.scoreList objectAtIndex:i]).score integerValue] < [score.score integerValue])
        {
            [self.scoreList insertObject:score atIndex:i];
            break;
        }
            
    }
    
    
    [currentUser setTotalScore:[NSNumber numberWithInteger:[[currentUser totalScore] integerValue] +  myScore]];
    
    if ([currentUser.level integerValue] < 3 && [currentUser.totalScore integerValue] >= kLevel3Score)
    {
        currentUser.level = [NSNumber numberWithInt:3];
        res = 3;
    }
    else if ([currentUser.level integerValue] < 2 && [currentUser.totalScore integerValue] >= kLevel2Score)
    {
        currentUser.level = [NSNumber numberWithInt:2];
        res = 2;
    }
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        //Handle the error
    }
    
    [self updateHighScoreList];
    
    return res;
}

-(void)updateHighScoreList
{
    NSMutableArray *highScores = [[NSMutableArray alloc] init];
    self.highScoreList = highScores;

    NSFetchRequest *request3 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity3 = [NSEntityDescription
                                    entityForName:@"User"
                                    inManagedObjectContext:managedObjectContext];
    [request3 setEntity:entity3];
    
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc]
                                         initWithKey:@"totalScore" ascending:NO];
    NSArray *sortDescriptors3 = [[NSArray alloc]
                                 initWithObjects:sortDescriptor3, nil];
    [request3 setSortDescriptors:sortDescriptors3];
    
    NSError *error3 = nil;
    NSMutableArray *mutableFetchResults3 = [[managedObjectContext
                                             executeFetchRequest:request3 error:&error3] mutableCopy];
    if (mutableFetchResults3 == nil) {
        // Handle the error.
    }
    
    [self setHighScoreList:mutableFetchResults3];
}

@end
