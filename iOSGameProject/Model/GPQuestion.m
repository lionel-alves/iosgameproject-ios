//
//  GPQuestion.m
//  iOSGameProject
//
//  Created by Lionel ALVES on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GPQuestion.h"

@implementation GPQuestion

@synthesize e1, e2, op, result;

- (id)initWithLevel:(int)level
{
    self = [super init];
    if (self) {
        e1 = arc4random() % (10 * level);
        e2 = arc4random() % (10 * level);
        op = arc4random() % 3;
        if (op == GPOperationEnumAddition)
            result = e1 + e2;
        else if (op == GPOperationEnumSubtraction)
            result = e1 - e2;
        else if (op == GPOperationEnumMultiplication)   
            result = e1 * e2;
    }
    return self;
}

- (NSString *)description {
    NSString *opStr = nil;
    
    if (op == GPOperationEnumAddition)
        opStr = @"+";
    else if (op == GPOperationEnumSubtraction)
        opStr = @"-";
    else if (op == GPOperationEnumMultiplication)   
        opStr = @"*";
    
    return [NSString stringWithFormat:@"%d%@%d", e1, opStr, e2];
}

@end
