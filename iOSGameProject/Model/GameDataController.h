//
//  GameDataController.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kLevel2Score 500
#define kLevel3Score 2000

@class User;
@class Score;

@interface GameDataController : NSObject

@property (nonatomic, copy) NSMutableArray *userList;
@property (nonatomic, copy) NSMutableArray *scoreList;
@property (nonatomic, copy) NSMutableArray *highScoreList;
@property (nonatomic, strong) NSManagedObjectContext
*managedObjectContext;

- (id)initWithContext:(NSManagedObjectContext *)context;

- (NSUInteger)countOfList;
- (User *)objectInListAtIndex:(NSUInteger)theIndex;
- (void)addUserWithName:(NSString *)inputUserName;
- (void)deleteUserWithIndexPath:(NSIndexPath *)indexPath;
- (int)addScoreWithUser:(User *)currentUser andScore:(NSInteger) myScore;
- (void)updateHighScoreList;

@end
