//
//  Score.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Score.h"
#import "User.h"


@implementation Score

@dynamic date;
@dynamic score;
@dynamic user;

@end
