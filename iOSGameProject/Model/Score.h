//
//  Score.h
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Score : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * score;
@property (nonatomic, retain) User *user;

@end
