//
//  User.m
//  iOSGameProject
//
//  Created by Jennifer Lim on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "User.h"
#import "Score.h"


@implementation User

@dynamic level;
@dynamic name;
@dynamic totalScore;
@dynamic scores;

@end
