//
//  GPQuestion.h
//  iOSGameProject
//
//  Created by Lionel ALVES on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum operationEnum {
        GPOperationEnumAddition,
        GPOperationEnumSubtraction,
    	GPOperationEnumMultiplication
    } GPOperationEnum;
@interface GPQuestion : NSObject

@property (nonatomic) NSInteger e1;
@property (nonatomic) NSInteger e2;
@property (nonatomic) GPOperationEnum op;
@property (nonatomic) NSInteger result;

- (id)initWithLevel:(int)level;
@end
